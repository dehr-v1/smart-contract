// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.8.3;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

/**
 * @dev This contract handles subscription features.
 */
contract Subscription is Ownable {
    IERC20 public dehrAddress;
    uint256 _paymentId;
    enum SubscribeMode {
        UPGRADE,
        EXTEND
    }

    struct PaymentReceipt {
        SubscribeMode mode;
        address addr;
        uint256 packageId;
        uint256 amount;
    }

    mapping(uint256 => PaymentReceipt) public receipt;

    event Upgrade(
        uint256 indexed paymentId,
        address indexed addr,
        uint256 indexed packageId,
        uint256 amount,
        string metadata
    );

    event Extend(
        uint256 indexed paymentId,
        address indexed addr,
        uint256 indexed packageId,
        uint256 amount,
        string metadata
    );

    /**
     * @dev Set the values for:
     * _dehrAddress: token address.
     */
    constructor(IERC20 _dehrAddress, uint256 _initPaymentId) {
        dehrAddress = _dehrAddress;
        _paymentId = _initPaymentId;
    }

    /**
     * @dev Upgrade
     */
    function upgrade(
        uint256 packageId,
        uint256 amount,
        string memory metadata
    ) external {
        require(
            amount > 0,
            "Subscription.upgrade: Amount should greater than 0"
        );

        // Get the money
        dehrAddress.transferFrom(msg.sender, address(this), amount);

        // Tracking receipt
        uint256 pId = _getPaymentId();
        PaymentReceipt storage p = receipt[pId];
        p.mode = SubscribeMode.UPGRADE;
        p.addr = msg.sender;
        p.packageId = packageId;
        p.amount = amount;

        emit Upgrade(pId, msg.sender, packageId, amount, metadata);
    }

    /**
     * @dev Extend
     */
    function extend(
        uint256 packageId,
        uint256 amount,
        string memory metadata
    ) external {
        require(
            amount > 0,
            "Subscription.extend: Amount should greater than 0"
        );

        // Get the money
        dehrAddress.transferFrom(msg.sender, address(this), amount);

        // Tracking receipt
        uint256 pId = _getPaymentId();
        PaymentReceipt storage p = receipt[pId];
        p.mode = SubscribeMode.EXTEND;
        p.addr = msg.sender;
        p.packageId = packageId;
        p.amount = amount;

        emit Extend(pId, msg.sender, packageId, amount, metadata);
    }

    function _getPaymentId() internal returns (uint256) {
        return ++_paymentId;
    }

    /**
     * @dev withdrawTokenByOwner
     * Used to withdraw all token to specific address
     */
    function withdrawTokenByOwner(address _to) external onlyOwner {
        uint256 amount = dehrAddress.balanceOf(address(this));
        dehrAddress.transfer(_to, amount);
    }
}
