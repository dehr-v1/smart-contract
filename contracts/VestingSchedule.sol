// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.8.3;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract VestingSchedule is Ownable {
    uint256 constant MONTH_DURATION = 60 * 60 * 24 * 30; // Second in 1 month
    uint256 constant BASE_PERCENT = 10_000;

    struct Account {
        uint256 debt;
        uint256 amount;
    }

    // Meta
    string public name;
    string public symbol;
    uint8 public decimals = 18;

    IERC20 public dehrToken;
    uint256 public startDate;

    uint256 public durationInMonth;
    uint256 public cliffReleasePercent;

    mapping(address => Account) public balances;

    event Claim(
        address indexed claimer,
        address indexed receipient,
        uint256 amount
    );
    event Start(uint256 startDate, uint256 endDate);

    modifier onlyStarted() {
        require(startDate > 0, "NOT_START");
        _;
    }

    constructor(
        IERC20 _dehr,
        uint256 _durationInMonth,
        uint256 _cliffReleasePercent,
        string memory _name,
        string memory _symbol
    ) {
        dehrToken = _dehr;
        durationInMonth = _durationInMonth;
        cliffReleasePercent = _cliffReleasePercent;

        name = _name;
        symbol = _symbol;
    }

    function setAccount(address _acc, uint256 _amount) public onlyOwner {
        Account storage acc = balances[_acc];
        acc.amount = _amount;
    }

    function setAccountBatch(address[] memory _accs, uint256[] memory _amounts)
        public
        onlyOwner
    {
        for (uint256 i = 0; i < _accs.length; i++) {
            setAccount(_accs[i], _amounts[i]);
        }
    }

    function startSchedule(uint256 _startDate) external onlyOwner {
        require(startDate == 0, "STARTED");

        if (_startDate == 0) {
            startDate = block.timestamp;
        } else {
            startDate = _startDate;
        }

        emit Start(startDate, startDate + durationInMonth * MONTH_DURATION);
    }

    function claim() external onlyStarted {
        _claim(msg.sender);
    }

    function claimFor(address[] memory _accounts) external onlyStarted {
        for (uint256 i = 0; i < _accounts.length; i++) {
            _claim(_accounts[i]);
        }
    }

    function _claim(address _to) internal {
        Account storage u = balances[_to];

        uint256 unlockedAmount = calculateUnlockAmount(u.amount, u.debt);
        require(unlockedAmount > 0, "TOO_SMALL");

        u.debt = u.debt + unlockedAmount;

        dehrToken.transfer(_to, unlockedAmount);

        emit Claim(_to, _to, unlockedAmount);
    }

    function calculateUnlockAmount(uint256 amount, uint256 debt)
        internal
        view
        returns (uint256)
    {
        uint256 dim = durationInMonth;

        uint256 duration = (block.timestamp - startDate) / MONTH_DURATION;

        if (duration >= dim) {
            return amount - debt;
        }

        uint256 cliffAmount = (amount * cliffReleasePercent) / BASE_PERCENT;
        uint256 unlockPerPeriod = (amount - cliffAmount) / dim;

        uint256 unlockedAmount = cliffAmount +
            unlockPerPeriod *
            duration -
            debt;
        return unlockedAmount;
    }

    function balanceOf(address _acc) external view returns (uint256) {
        return balances[_acc].amount - balances[_acc].debt;
    }

    function estimateUnlocked(address _acc) external view returns (uint256) {
        Account storage u = balances[_acc];

        return calculateUnlockAmount(u.amount, u.debt);
    }
}
