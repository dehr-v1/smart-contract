// SPDX-License-Identifier: MIT

pragma solidity 0.8.3;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./Staking.sol";

/**
 * @dev This contract handles staking features since Oct 2022
 */
contract StakingOctober is Ownable {
    // Contracts
    IERC20 public dehrToken;
    Staking public stakingIDO;

    // Constants
    uint256 constant A_DAY_IN_SECOND = 24 * 60 * 60; // A day = 86400 seconds
    uint256 constant A_YEAR_IN_DAY = 30 * 12; // A year = 360 days
    uint256 constant BASE_PERCENT = 100_000; // 5_000 ~ 5%

    // Configurations
    uint256 public tenorInDay = 30; // Default: 30 days
    uint256 public apyPercent = 5_000; // Default: 5000 is 5%
    uint256 public stakingLimit = 0; // Default: 0 is no limit
    uint256 public withdrawFeePercent = 0; // Default: 0%
    uint256 public dissolveChunk = 50; // Default: Dissolution will refund 50 holders each transaction

    // Insight
    uint256 public totalStakingAmount = 0;

    // Database - StakeHolders
    struct StakeHolder {
        uint256 stakeDate;
        uint256 stakeAmount;
        uint256 lastClaimDate;
        bool isMigrated;
    }
    mapping(address => StakeHolder) public stakeHolders;

    // Database - Dissolutions
    address[] investers;
    uint256 public nextDissolveIndex = 0;
    bool public isDissolved = false;

    // Database - Operators
    mapping(address => bool) public operators;

    // Events
    event SetOperator(address indexed operator, bool enable);
    event Migrate(
        address indexed stakeHolder,
        uint256 stakeAmount,
        uint256 rewardAmount,
        uint256 newStakeAmount,
        uint256 newStartDate
    );
    event Stake(
        address indexed stakeHolder,
        uint256 stakeAmount,
        uint256 totalStakingAmount,
        uint256 stakeDate
    );
    event Reward(
        address indexed stakeHolder,
        uint256 interestAmount,
        uint256 calculateFrom,
        uint256 calculateTo,
        uint256 cycles
    );
    event Withdraw(
        address indexed stakeHolder,
        uint256 stakeAmount,
        uint256 fee,
        uint256 totalReturned
    );
    event ConfigurationChanged(
        uint256 tenorInDay,
        uint256 apyPercent,
        uint256 stakingLimit,
        uint256 withdrawFeePercent,
        uint256 dissolveChunk
    );
    event Refund(address indexed stakeHolder, uint256 stakeAmount);
    event Dissolution(uint256 currentIndex, uint256 totalHolder);

    /**
     * @dev Constructor
     */
    constructor(Staking _stakingIDO) {
        stakingIDO = _stakingIDO;
        dehrToken = _stakingIDO.dehrToken();

        operators[msg.sender] = true;
        emit SetOperator(msg.sender, true);
    }

    modifier moreThanZero(uint256 _value) {
        require(_value > 0, "StakingOctober: Value must be more than zero");
        _;
    }

    modifier onlyOperator(address _acc) {
        require(operators[_acc], "StakingOctober: You are not an operator");
        _;
    }

    modifier notDissolved() {
        require(!isDissolved, "StakingOctober: Contract is dissolved");
        _;
    }

    /**
     * @notice internal function to DRY code
     */
    function _emitConfigChanged() internal {
        emit ConfigurationChanged(
            tenorInDay,
            apyPercent,
            stakingLimit,
            withdrawFeePercent,
            dissolveChunk
        );
    }

    /**
     * @dev Used by the contract owner
     * @param _operator address Address of the operator
     * @param _enable bool true is to enable, false is to disable
     */
    function setOperator(address _operator, bool _enable) external onlyOwner {
        operators[_operator] = _enable;
        emit SetOperator(_operator, _enable);
    }

    /**
     * @dev Used by the operator
     * @param _days The number of days use for calculating an interest cycle, must greater than 0
     */
    function setTenorInDay(uint256 _days)
        external
        onlyOperator(msg.sender)
        moreThanZero(_days)
    {
        tenorInDay = _days;
        _emitConfigChanged();
    }

    /**
     * @dev Used by the operator
     * @param _percents uint256 The percent use for calculating the interest per year
     * @notice _percents need to multiply with BASE_PERCENT: 5% * 100.000 = 5000 <= this is the input
     */
    function setApyPercent(uint256 _percents)
        external
        onlyOperator(msg.sender)
        moreThanZero(_percents)
    {
        apyPercent = _percents;
        _emitConfigChanged();
    }

    /**
     * @dev Used by the operator
     * @param _limit uint256 The limit for total staking at a time, 0 is no limit
     */
    function setStakingLimit(uint256 _limit) external onlyOperator(msg.sender) {
        stakingLimit = _limit;
        _emitConfigChanged();
    }

    /**
     * @dev Used by the operator
     * @param _percents uint256 The percent use for calculating the interest per year
     * @notice _percents need to multiply with BASE_PERCENT: 0.6% * 100.000 = 600 <= this is the input
     */
    function setWithdrawFeePercent(uint256 _percents)
        external
        onlyOperator(msg.sender)
    {
        withdrawFeePercent = _percents;
        _emitConfigChanged();
    }

    /**
     * @dev Used by the owner
     * @param _chunk uint256 Number of holders will be refunded in a transaction
     * @notice _chunk need to be adapt with the total gasLimit of a transaction
     */
    function setDissolveChunk(uint256 _chunk)
        external
        onlyOwner
        moreThanZero(_chunk)
    {
        dissolveChunk = _chunk;
        _emitConfigChanged();
    }

    /**
     * @dev public view
     */
    function getConfigurations()
        external
        view
        returns (
            uint256,
            uint256,
            uint256,
            uint256,
            uint256
        )
    {
        return (
            tenorInDay,
            apyPercent,
            stakingLimit,
            withdrawFeePercent,
            dissolveChunk
        );
    }

    /**
     * @dev Used by the operator
     * @param _holders address[] list of stake holders from old contract
     * @notice The function will ignore holders who don't actually have invested fund in the old contract
     */
    function migrate(address[] calldata _holders)
        external
        onlyOperator(msg.sender)
        notDissolved
    {
        require(
            _holders.length > 0,
            "StakingOctober: Require at least 1 holder"
        );

        for (uint256 i = 0; i < _holders.length; i++) {
            address _addr = _holders[i];
            StakeHolder storage _holder = stakeHolders[_addr];
            if (!_holder.isMigrated) {
                (, uint256 _stakeAmount, ) = stakingIDO.stakeInfo(_addr);
                if (_stakeAmount > 0) {
                    uint256 _rewardAmount = stakingIDO.calculateReward(_addr);
                    uint256 _totalAmount = _stakeAmount + _rewardAmount;

                    _holder.stakeDate = block.timestamp;
                    _holder.stakeAmount = _totalAmount;
                    _holder.lastClaimDate = block.timestamp;
                    _holder.isMigrated = true;

                    totalStakingAmount += _totalAmount;
                    investers.push(_addr);

                    emit Migrate(
                        _addr,
                        _stakeAmount,
                        _rewardAmount,
                        _holder.stakeAmount,
                        _holder.stakeDate
                    );
                }
            }
        }
    }

    /**
     * @dev Used by the stake holder
     * @param _amount uint256 The amount of token will be transfered to this contract
     * @notice The function will release the reward if user have staked before and the startDate will be reset to the current time
     */
    function stake(uint256 _amount)
        external
        moreThanZero(_amount)
        notDissolved
    {
        if (stakingLimit > 0) {
            require(
                totalStakingAmount + _amount <= stakingLimit,
                "StakingOctober: Exceed staking limit"
            );
        }

        StakeHolder storage _holder = stakeHolders[msg.sender];

        // Tracking holder list for future dissolution
        if (_holder.stakeDate == 0) {
            investers.push(msg.sender);
        }

        // Reward before recalculating next cycle
        if (_holder.stakeAmount > 0) {
            _releaseReward(msg.sender);
        }

        // Transfer the token
        dehrToken.transferFrom(msg.sender, address(this), _amount);
        
        // Update database
        _holder.stakeDate = block.timestamp;
        _holder.stakeAmount += _amount;
        _holder.lastClaimDate = block.timestamp;

        // Update insight
        totalStakingAmount += _amount;

        emit Stake(msg.sender, _amount, _holder.stakeAmount, block.timestamp);
    }

    /**
     * @dev Used by the stake holder to claim the interest
     */
    function claim() external notDissolved {
        StakeHolder storage _holder = stakeHolders[msg.sender];
        require(_holder.stakeAmount > 0, "StakingOctober: Not staked");

        uint256 _cycles = (block.timestamp - _holder.lastClaimDate) /
            A_DAY_IN_SECOND /
            tenorInDay;
        require(_cycles > 0, "StakingOctober: Require minimal stake duration");

        uint256 _reward = (_holder.stakeAmount * apyPercent * _cycles) /
            (BASE_PERCENT * A_YEAR_IN_DAY);
        uint256 _claimDuration = _cycles * tenorInDay * A_DAY_IN_SECOND;

        dehrToken.transfer(msg.sender, _reward);
        emit Reward(
            msg.sender,
            _reward,
            _holder.lastClaimDate,
            _holder.lastClaimDate + _claimDuration,
            _cycles
        );

        // Update state for next claim
        _holder.lastClaimDate += _claimDuration;
    }

    /**
     * @dev internal
     * @param _account address The address of the stake holder
     */
    function _releaseReward(address _account) internal {
        StakeHolder storage _holder = stakeHolders[_account];
        if (_holder.stakeAmount == 0) return;

        uint256 _cycles = (block.timestamp - _holder.lastClaimDate) /
            A_DAY_IN_SECOND /
            tenorInDay;
        if (_cycles == 0) return;

        uint256 _reward = (_holder.stakeAmount * apyPercent * _cycles) /
            (BASE_PERCENT * A_YEAR_IN_DAY);
        uint256 _claimDuration = _cycles * tenorInDay * A_DAY_IN_SECOND;

        dehrToken.transfer(_account, _reward);
        emit Reward(
            _account,
            _reward,
            _holder.lastClaimDate,
            _holder.lastClaimDate + _claimDuration,
            _cycles
        );

        // Update state for next claim
        _holder.lastClaimDate += _claimDuration;
    }

    /**
     * @dev Used by the stake holder
     */
    function withdraw() external notDissolved {
        StakeHolder storage _holder = stakeHolders[msg.sender];
        require(_holder.stakeAmount > 0, "StakingOctober: Not staked");

        _releaseReward(msg.sender);

        uint256 _stakeAmount = _holder.stakeAmount;
        uint256 _fee = (_stakeAmount * withdrawFeePercent) / BASE_PERCENT;
        uint256 _returnAmount = _stakeAmount - _fee;

        // Reset state
        _holder.stakeAmount = 0;

        // Update insight
        totalStakingAmount -= _stakeAmount;

        dehrToken.transfer(msg.sender, _returnAmount);
        emit Withdraw(msg.sender, _stakeAmount, _fee, _returnAmount);
    }

    /**
     * @dev external view
     * @return uint256 interest amount at the current time
     */
    function calculateReward(address _account) external view returns (uint256) {
        StakeHolder storage _holder = stakeHolders[_account];
        if (_holder.stakeAmount == 0) return 0;

        uint256 _cycles = (block.timestamp - _holder.lastClaimDate) /
            A_DAY_IN_SECOND /
            tenorInDay;
        if (_cycles == 0) return 0;

        return
            (_holder.stakeAmount * apyPercent * _cycles) /
            (BASE_PERCENT * A_YEAR_IN_DAY);
    }

    /**
     * @dev external view
     * @return uint256 total stake holders in this contract
     */
    function getTotalHolders() external view returns (uint256) {
        return investers.length;
    }

    /**
     * @dev internal
     * @param _account address The address of the stake holder
     * @notice This function is to support dissolution situation
     */
    function _refund(address _account) internal {
        StakeHolder storage _holder = stakeHolders[_account];
        if (_holder.stakeAmount == 0) return;

        _releaseReward(_account);

        uint256 _stakeAmount = _holder.stakeAmount;

        // Reset state
        _holder.stakeAmount = 0;

        // Update insight
        totalStakingAmount -= _stakeAmount;

        dehrToken.transfer(_account, _stakeAmount);

        emit Refund(_account, _stakeAmount);
    }

    /**
     * @dev Used by the contract owner
     * @notice This funcion is to refund all stake and interest to stake holders
     * @notice After using this function, all functions from investers will be stopped
     */
    function dissolve() external onlyOwner {
        // Update dissolution status
        isDissolved = true;
        
        uint256 _max = investers.length - nextDissolveIndex;
        require(_max > 0, "StakingOctober: Finished dissolution");

        if (_max > dissolveChunk) {
            _max = dissolveChunk;
        }

        for (uint256 i = 0; i < _max; i++) {
            _refund(investers[nextDissolveIndex + i]);
        }

        // Update dissolve index
        nextDissolveIndex += _max;

        emit Dissolution(nextDissolveIndex, investers.length);
    }

    /**
     * @dev Used by the contract owner
     * @notice This function will withdraw all token in this contract
     */
    function withdrawTokenByOwner(address _to) external onlyOwner {
        uint256 _amount = dehrToken.balanceOf(address(this));
        dehrToken.transfer(_to, _amount);
    }
}
