// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.8.3;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract Distributor is Ownable {
    function transferToken(
        IERC20 token,
        address[] calldata accounts,
        uint256[] calldata amounts
    ) external onlyOwner {
        require(accounts.length == amounts.length, "DIFF LEN");

        for (uint256 i = 0; i < accounts.length; i++) {
            token.transfer(accounts[i], amounts[i]);
        }
    }

    function transferTokenFrom(
        IERC20 token,
        address tokenOwner,
        address[] calldata accounts,
        uint256[] calldata amounts
    ) external onlyOwner {
        require(accounts.length == amounts.length, "DIFF LEN");

        for (uint256 i = 0; i < accounts.length; i++) {
            token.transferFrom(tokenOwner, accounts[i], amounts[i]);
        }
    }

    function transferNativeToken(
        address payable[] calldata accounts,
        uint256[] calldata amounts
    ) external onlyOwner {
        require(accounts.length == amounts.length, "DIFF LEN");

        for (uint256 i = 0; i < accounts.length; i++) {
            accounts[i].transfer(amounts[i]);
        }
    }

    // Receive native token
    receive() external payable {}
}
