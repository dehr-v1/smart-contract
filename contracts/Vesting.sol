// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.8.3;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract Vesting is Ownable {
    uint256 constant MONTH_DURATION = 30 * 1 days; // Second in 1 month
    uint256 constant BASE_PERCENT = 10_000;

    // Meta
    string public name;

    IERC20 public dehrToken;
    uint256 public startDate;

    uint256 public immutable cliffDurationMonth;
    uint256 public immutable cliffReleasePercent;

    uint256 public immutable periodDurationMonth;
    uint256 public immutable periodReleasePercent;

    uint256 public immutable totalLockAmount;

    uint256 public lastWithdrawTime;

    event Withdraw(address to, uint256 amount);
    event Start(uint256 startDate);

    modifier onlyStarted() {
        require(startDate > 0 && startDate < block.timestamp, "NOT START");
        _;
    }

    constructor(
        IERC20 _dehr,
        uint256 _cliffDurationMonth,
        uint256 _cliffReleasePercent,
        uint256 _periodDurationMonth,
        uint256 _periodReleasePercent,
        uint256 _totalLockAmount,
        string memory _name
    ) {
        dehrToken = _dehr;
        cliffDurationMonth = _cliffDurationMonth;
        cliffReleasePercent = _cliffReleasePercent;

        periodDurationMonth = _periodDurationMonth;
        periodReleasePercent = _periodReleasePercent;

        totalLockAmount = _totalLockAmount;

        name = _name;
    }

    function withdraw(address _to) external onlyOwner onlyStarted {
        uint256 unlockedAmount;
        uint256 unlockedDueDate;

        if (lastWithdrawTime == 0) {
            // Cliff
            unlockedDueDate = startDate + cliffDurationMonth * MONTH_DURATION;
            unlockedAmount =
                (totalLockAmount * cliffReleasePercent) /
                BASE_PERCENT;
        } else {
            // Period
            unlockedAmount =
                (totalLockAmount * periodReleasePercent) /
                BASE_PERCENT;
            unlockedDueDate =
                lastWithdrawTime +
                periodDurationMonth *
                MONTH_DURATION;
        }

        require(unlockedDueDate < block.timestamp, "NOT MATURE");

        lastWithdrawTime = block.timestamp;
        dehrToken.transfer(_to, unlockedAmount);

        emit Withdraw(_to, unlockedAmount);
    }

    function startVesting(uint256 _startDate) external onlyOwner {
        require(startDate == 0, "STARTED");

        if (_startDate == 0) {
            startDate = block.timestamp;
        } else {
            startDate = _startDate;
        }

        emit Start(startDate);
    }

    function getCliffDate() external view returns (uint256) {
        return startDate + cliffDurationMonth * MONTH_DURATION;
    }

    function getWithdrawDate() external view returns (uint256) {
        if (lastWithdrawTime == 0) {
            return startDate + cliffDurationMonth * MONTH_DURATION;
        } else {
            return lastWithdrawTime + periodDurationMonth * MONTH_DURATION;
        }
    }
}
