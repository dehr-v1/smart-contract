// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.8.3;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

interface IBPContract {
    function protect(
        address sender,
        address receiver,
        uint256 amount
    ) external;
}

contract DeHR is ERC20, Ownable {
    uint256 private constant DECIMALS = 18;
    uint256 private constant MAX_SUPPLY = 1_000_000_000 * 10**DECIMALS; // 1B

    IBPContract public bpContract;
    bool public bpEnabled;
    bool public bpDisabledForever;

    mapping(address => bool) public operators;

    struct Proposal {
        address proposer;
        address to;
        uint256 amount;
        uint256 agreeCount;
    }

    Proposal[] public proposals;
    Proposal[] public changeOperatorProposals;

    event Propose(
        uint256 indexed proposalId,
        address indexed to,
        uint256 amount
    );
    event Accept(uint256 proposalId);

    event SetOperator(address indexed operator, bool enable);
    event ProposeOperator(
        uint256 indexed proposalId,
        address indexed operator,
        uint256 status
    );

    modifier onlyOperator(address _acc) {
        require(operators[_acc], "NOT OPERATOR");
        _;
    }

    constructor(
        address _acc1,
        address _acc2,
        address _acc3
    ) ERC20("DeHR", "DHR") {
        operators[_acc1] = true;
        operators[_acc2] = true;
        operators[_acc3] = true;

        emit SetOperator(_acc1, true);
        emit SetOperator(_acc2, true);
        emit SetOperator(_acc3, true);
    }

    function propose(address _to, uint256 _amount)
        external
        onlyOperator(msg.sender)
        returns (uint256)
    {
        Proposal memory p;
        p.to = _to;
        p.amount = _amount;
        p.agreeCount = 1;
        p.proposer = msg.sender;

        proposals.push(p);
        uint256 id = proposals.length - 1;
        emit Propose(id, _to, _amount);

        return id;
    }

    function accept(uint256 _proposalId) external onlyOperator(msg.sender) {
        Proposal storage p = proposals[_proposalId];

        require(p.agreeCount == 1, "INVALID");
        require(msg.sender != p.proposer, "INVALID SENDER");

        p.agreeCount += 1;

        _mint(p.to, p.amount);

        require(totalSupply() <= MAX_SUPPLY, "MAX SUPPLY");

        emit Accept(_proposalId);
    }

    // status = 1: enable
    // status = 2: disable
    function proposeOperator(address _operator, uint256 _status)
        external
        onlyOperator(msg.sender)
        returns (uint256)
    {
        require(_status == 1 || _status == 2, "INVALID");

        Proposal memory p;
        p.to = _operator;
        p.amount = _status;
        p.agreeCount = 1;
        p.proposer = msg.sender;

        changeOperatorProposals.push(p);
        uint256 id = changeOperatorProposals.length - 1;
        emit ProposeOperator(id, _operator, _status);

        return id;
    }

    function acceptOperator(uint256 _proposalId)
        external
        onlyOperator(msg.sender)
    {
        Proposal storage p = changeOperatorProposals[_proposalId];

        require(p.agreeCount == 1, "INVALID");
        require(msg.sender != p.proposer, "INVALID SENDER");

        p.agreeCount += 1;

        if (p.amount == 1) {
            operators[p.to] = true;
            emit SetOperator(p.to, true);
        } else {
            operators[p.to] = false;
            emit SetOperator(p.to, false);
        }
    }

    function burn(uint256 amount) external {
        _burn(msg.sender, amount);
    }

    function setBPContract(address addr) public onlyOwner {
        require(addr != address(0), "BP adress cannot be 0x0");

        bpContract = IBPContract(addr);
    }

    function setBPEnabled(bool enabled) public onlyOwner {
        bpEnabled = enabled;
    }

    function setBPDisableForever() public onlyOwner {
        require(!bpDisabledForever, "Bot protection disabled");

        bpDisabledForever = true;
    }

    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal override {
        if (bpEnabled && !bpDisabledForever) {
            bpContract.protect(from, to, amount);
        }
        super._beforeTokenTransfer(from, to, amount);
    }
}
