// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.8.3;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

/**
 * @dev This contract handles staking features.
 */
contract StakingMembership is Ownable {
    IERC20 public dehrToken;
    uint256 public duration = 30 days;

    struct Account {
        uint256 lockTo;
        uint256 amount;
    }

    mapping(address => Account) public stakeInfo;

    event Stake(
        address indexed addr,
        uint256 amount,
        uint256 totalAmount,
        uint256 lockTo
    );
    event Unstake(address indexed addr, uint256 amount, uint256 totalAmount);

    /**
     * @dev Set the values for:
     * _dehrToken: dehr token address.
     */
    constructor(IERC20 _dehrToken) {
        dehrToken = _dehrToken;
    }

    /**
     * @dev Stake
     */
    function stake(uint256 amount) external {
        dehrToken.transferFrom(msg.sender, address(this), amount);

        Account storage acc = stakeInfo[msg.sender];

        uint256 totalAmount = acc.amount + amount;
        uint256 lockTo = block.timestamp + duration;

        acc.amount = totalAmount;
        acc.lockTo = lockTo;

        emit Stake(msg.sender, amount, totalAmount, lockTo);
    }

    /**
     * @dev Unstake
     */
    function unstake(uint256 amount) external {
        Account storage acc = stakeInfo[msg.sender];
        uint256 currentAmount = acc.amount;
        require(acc.lockTo < block.timestamp, "LOCKED");
        require(amount <= currentAmount, "INVALID AMOUNT");

        acc.amount = currentAmount - amount;

        dehrToken.transfer(msg.sender, amount);
        emit Unstake(msg.sender, amount, currentAmount - amount);
    }

    /**
     * @dev Change the staking duration.
     */
    function changeStakingDuration(uint256 newDuration) external onlyOwner {
        duration = newDuration;
    }

    /**
     * @dev Change the stake token.
     */
    function changeStakeToken(IERC20 newStakeToken) external onlyOwner {
        dehrToken = newStakeToken;
    }
}
