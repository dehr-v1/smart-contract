// SPDX-License-Identifier: UNLICENSED

pragma solidity 0.8.3;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

/**
 * @dev This contract handles account registration and exchange data flow.
 */
contract ExchangeData is Ownable {
    uint256 constant feeBase = 10000;

    enum STATUS {
        INITIALIZED,
        ACCEPTED,
        REJECTED,
        CANCELLED,
        PAID
    }

    mapping(address => bool) public operators;

    IERC20 public dehrToken;
    uint256 public feePercent;
    address public feeAddress;

    struct Account {
        bytes publicKey;
        bytes32 dataChecksum;
        uint256 price;
    }

    struct Request {
        address sender;
        address recipient;
        uint128 tokenAmount;
        STATUS status;
        uint256 fee;
    }

    mapping(address => Account) public accounts;
    Request[] public requests;

    modifier onlyOperator(address _acc) {
        require(operators[_acc], "NOT OPERATOR");
        _;
    }

    event Register(
        address indexed account,
        bytes pubkey,
        bytes32 checksum,
        uint256 price
    );

    event SendRequest1(
        uint256 indexed id,
        address indexed sender,
        address indexed recipient,
        uint256 tokenAmount,
        uint256 fee
    );

    event SendRequest2(
        uint256 indexed id,
        address indexed sender,
        address indexed recipient,
        uint256 tokenAmount,
        uint256 fee
    );
    event AcceptRequest(uint256 id);
    event CancelRequest(uint256 id);
    event RejectRequest(uint256 id);

    event SetOperator(address indexed addr, bool status);

    event UpdateAccountPrice(address indexed addr, uint256 price);
    event UpdateAccountChecksum(address indexed addr, bytes32 checksum);
    event UpdateAccountPubkey(address indexed addr, bytes pubkey);

    /**
     * @dev Set the values for:
     * _dehrToken: dehr token address.
     * _feePercent: fee of exchange data.
     * _feeAddress: the fee receiver address.
     */
    constructor(
        IERC20 _dehrToken,
        uint256 _feePercent,
        address _feeAddress
    ) {
        dehrToken = _dehrToken;
        feePercent = _feePercent;
        feeAddress = _feeAddress;
    }

    /**
     * @dev Record account information.
     */
    function register(
        address _account,
        bytes memory _pubKey,
        bytes32 _checksum,
        uint256 _price
    ) external onlyOperator(msg.sender) {
        _registerAccount(_account, _pubKey, _checksum, _price);
    }

    /**
     * @dev Record multiple account information.
     */
    function registerBatch(
        address[] memory _accounts,
        bytes[] memory _pubKeys,
        bytes32[] memory _checksums,
        uint256[] memory _prices
    ) external onlyOperator(msg.sender) {
        for (uint256 i = 0; i < _accounts.length; i++) {
            _registerAccount(
                _accounts[i],
                _pubKeys[i],
                _checksums[i],
                _prices[i]
            );
        }
    }

    function _registerAccount(
        address _account,
        bytes memory _pubKey,
        bytes32 _checksum,
        uint256 _price
    ) internal {
        Account memory a = Account(_pubKey, _checksum, _price);
        accounts[_account] = a;
        emit Register(_account, _pubKey, _checksum, _price);
    }

    /**
     * @dev Send request to {_account}.
     * This function transfer token to this smart contract.
     */
    function request1(address _account, uint256 _amount)
        external
        returns (uint256 id)
    {
        uint256 _expectedPrice = accounts[_account].price;
        require(_amount >= _expectedPrice, "ExchangeData: Insufficient amount");

        // Contract accept the base price settle by the seller, not the amount sender sent
        uint256 fee = _calculateFee(_expectedPrice);

        Request memory r = Request(
            msg.sender,
            _account,
            uint128(_expectedPrice),
            STATUS.INITIALIZED,
            fee
        );
        id = requests.length;
        requests.push(r);

        // Transfer token from sender to this contract
        dehrToken.transferFrom(msg.sender, address(this), _expectedPrice);

        emit SendRequest1(id, msg.sender, _account, _expectedPrice, fee);
    }

    /**
     * @dev Accept a request and get token.
     *
     * This function also send fee to the fee address.
     */
    function accept(uint256 _requestId) external {
        Request storage r = requests[_requestId];

        require(
            r.status == STATUS.INITIALIZED,
            "ExchangeData: Invalid request status"
        );
        require(r.recipient == msg.sender, "ExchangeData: Invalid recipient");

        r.status = STATUS.ACCEPTED;

        // Transfer token to request recipient
        uint256 tokenAmount = r.tokenAmount;
        uint256 fee = r.fee;

        if (tokenAmount > 0) {
            dehrToken.transfer(msg.sender, tokenAmount - fee);
            if (fee > 0) {
                dehrToken.transfer(feeAddress, fee);
            }
        }

        emit AcceptRequest(_requestId);
    }

    /**
     * @dev Send request to {_account}.
     * This function transfers token directly to reciepient.
     */
    function request2(address _account, uint256 _amount)
        external
        returns (uint256 id)
    {
        uint256 _expectedPrice = accounts[_account].price;
        require(_amount >= _expectedPrice, "ExchangeData: Insufficient amount");

        // Contract accept the base price settle by the seller, not the amount sender sent
        uint256 fee = _calculateFee(_expectedPrice);

        Request memory r = Request(
            msg.sender,
            _account,
            uint128(_expectedPrice),
            STATUS.PAID,
            fee
        );
        id = requests.length;
        requests.push(r);

        // Transfer token to request recipient
        if (_expectedPrice > 0) {
            dehrToken.transferFrom(msg.sender, _account, _expectedPrice - fee);
            if (fee > 0) {
                dehrToken.transferFrom(msg.sender, feeAddress, fee);
            }
        }

        emit SendRequest2(id, msg.sender, _account, _expectedPrice, fee);
    }

    /**
     * @dev Buyer cancel a request.
     */
    function cancelRequest(uint256 _requestId) external {
        Request storage r = requests[_requestId];
        require(r.sender == msg.sender, "ExchangeData: Invalid sender");
        require(
            r.status == STATUS.INITIALIZED,
            "ExchangeData: Invalid request status"
        );

        r.status = STATUS.CANCELLED;
        dehrToken.transfer(r.sender, r.tokenAmount);
        emit CancelRequest(_requestId);
    }

    /**
     * @dev Seller reject a request.
     */
    function rejectRequest(uint256 _requestId) external {
        Request storage r = requests[_requestId];
        require(r.recipient == msg.sender, "ExchangeData: Invalid recipient");
        require(
            r.status == STATUS.INITIALIZED,
            "ExchangeData: Invalid request status"
        );

        r.status = STATUS.REJECTED;
        dehrToken.transfer(r.sender, r.tokenAmount);
        emit RejectRequest(_requestId);
    }

    //////////////////////////////////////////////////
    function updatePrice(uint256 newPrice) external {
        Account storage acc = accounts[msg.sender];
        require(acc.dataChecksum > 0, "ExchangeData: Invalid account");
        acc.price = newPrice;
        emit UpdateAccountPrice(msg.sender, newPrice);
    }

    function updatePubkey(bytes memory pubkey) external {
        Account storage acc = accounts[msg.sender];
        require(acc.dataChecksum > 0, "ExchangeData: Invalid account");
        acc.publicKey = pubkey;
        emit UpdateAccountPubkey(msg.sender, pubkey);
    }

    function updateChecksum(address account, bytes32 checksum)
        public
        onlyOperator(msg.sender)
    {
        Account storage acc = accounts[account];
        acc.dataChecksum = checksum;
        emit UpdateAccountChecksum(account, checksum);
    }

    function updateChecksums(
        address[] memory addresses,
        bytes32[] memory checksums
    ) external {
        for (uint256 i = 0; i < addresses.length; i++) {
            updateChecksum(addresses[i], checksums[i]);
        }
    }

    //////////////////////////////////////////////////
    /**
     * @dev Change status of an operator.
     */
    function setOperator(address _addr, bool status) external onlyOwner {
        operators[_addr] = status;
        emit SetOperator(_addr, status);
    }

    /**
     * @dev Change the fee address.
     */
    function setFeeAddress(address _feeAddr) external onlyOperator(msg.sender) {
        feeAddress = _feeAddr;
    }

    function setFeePercent(uint256 _feePercent)
        external
        onlyOperator(msg.sender)
    {
        feePercent = _feePercent;
    }

    function _calculateFee(uint256 _amount) internal view returns (uint256) {
        return (_amount * feePercent) / feeBase;
    }
}
