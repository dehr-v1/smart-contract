// SPDX-License-Identifier: MIT

pragma solidity 0.8.3;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

/**
 * @dev This contract handles staking features.
 */
contract Staking is Ownable {
    IERC20 public dehrToken;

    uint256 constant HOUR_IN_SEC = 60 * 60; // 3600 seconds
    uint256 constant YEAR_IN_HOUR = 360 * 24; // 8640 hours
    uint256 constant BASE_PERCENT = 10_000;

    uint256 public immutable maxStakeAmount; // 5.000.000 * 10^18 DeHR
    uint256 public immutable apyPercent; // 3000 for 30%
    uint256 public immutable withdrawFeePercent; // 60 for 0.6%
    uint256 public totalStakedAmount;
    string public name;

    struct Account {
        uint256 startDate;
        uint256 stakeAmount;
        uint256 claimedHours;
    }

    mapping(address => Account) public stakeInfo;

    event Stake(
        address indexed addr,
        uint256 amount,
        uint256 totalAmount,
        uint256 startDate
    );

    event Reward(address indexed addr, uint256 amount, uint256 rewardHours);

    event Withdraw(
        address indexed addr,
        uint256 stakeAmount,
        uint256 rewardAmount,
        uint256 totalReturned
    );

    /**
     * @dev Set the values for:
     * _dehrToken: dehr token address.
     */
    constructor(
        IERC20 _dehrToken,
        uint256 _maxStakeAmount,
        uint256 _apyPercent,
        uint256 _withdrawFeePercent,
        string memory _name
    ) {
        dehrToken = _dehrToken;
        maxStakeAmount = _maxStakeAmount;
        apyPercent = _apyPercent;
        withdrawFeePercent = _withdrawFeePercent;
        name = _name;
    }

    /**
     * @dev Stake
     */
    function stake(uint256 amount) external {
        require(amount > 0, "Staking: Stake amount should greater than 0");

        uint256 nextTotal = totalStakedAmount + amount;
        require(
            nextTotal <= maxStakeAmount,
            "Staking: Exceed maximum staking amount"
        );

        dehrToken.transferFrom(msg.sender, address(this), amount);

        Account storage acc = stakeInfo[msg.sender];

        // Perform reward before continuing stake
        if (acc.stakeAmount > 0) {
            _releaseReward(msg.sender);
        }

        acc.stakeAmount += amount;
        acc.startDate = block.timestamp;
        acc.claimedHours = 0;

        totalStakedAmount += amount;

        emit Stake(msg.sender, amount, acc.stakeAmount, acc.startDate);
    }

    /**
     * @dev Withdraw
     */
    function withdraw() external {
        Account storage acc = stakeInfo[msg.sender];
        require(acc.stakeAmount > 0, "Staking: Not staked");

        _releaseReward(msg.sender);

        uint256 stakeAmount = acc.stakeAmount;
        uint256 fee = (stakeAmount * withdrawFeePercent) / BASE_PERCENT;
        uint256 totalReturned = stakeAmount - fee;

        // Reset state
        acc.stakeAmount = 0;
        acc.startDate = 0;
        acc.claimedHours = 0;
        totalStakedAmount -= stakeAmount;

        dehrToken.transfer(msg.sender, totalReturned);
        emit Withdraw(msg.sender, stakeAmount, fee, totalReturned);
    }

    /**
     * @dev Claim reward
     */
    function claim() external {
        Account storage acc = stakeInfo[msg.sender];
        require(acc.stakeAmount > 0, "Staking: Not staked");

        uint256 stakeHours = (block.timestamp - acc.startDate) / HOUR_IN_SEC;
        uint256 rewardHours = stakeHours - acc.claimedHours;

        require(rewardHours > 0, "Staking: Require minimal stake duration");

        uint256 rewardAmount = (acc.stakeAmount * apyPercent * rewardHours) /
            (BASE_PERCENT * YEAR_IN_HOUR);

        // Update state for next claim
        acc.claimedHours = stakeHours;

        dehrToken.transfer(msg.sender, rewardAmount);
        emit Reward(msg.sender, rewardAmount, rewardHours);
    }

    function _releaseReward(address _account) internal {
        Account storage acc = stakeInfo[_account];
        if (acc.stakeAmount == 0) return;

        uint256 stakeHours = (block.timestamp - acc.startDate) / HOUR_IN_SEC;
        uint256 rewardHours = stakeHours - acc.claimedHours;
        if (rewardHours == 0) return;

        uint256 rewardAmount = (acc.stakeAmount * apyPercent * rewardHours) /
            (BASE_PERCENT * YEAR_IN_HOUR);

        // Update state for next claim
        acc.claimedHours = stakeHours;

        dehrToken.transfer(_account, rewardAmount);
        emit Reward(_account, rewardAmount, rewardHours);
    }

    /**
     * @dev calculateReward
     */
    function calculateReward(address _account) external view returns (uint256) {
        Account storage acc = stakeInfo[_account];
        if (acc.stakeAmount == 0) return 0;

        uint256 stakeHours = (block.timestamp - acc.startDate) / HOUR_IN_SEC;
        uint256 rewardHours = stakeHours - acc.claimedHours;
        if (rewardHours == 0) return 0;

        return
            (acc.stakeAmount * apyPercent * rewardHours) /
            (BASE_PERCENT * YEAR_IN_HOUR);
    }

    /**
     * @dev withdrawTokenByOwner
     * Used to withdraw all token in emergency case or withdraw all remaining fee/interest
     */
    function withdrawTokenByOwner(address _to) external onlyOwner {
        uint256 amount = dehrToken.balanceOf(address(this));
        dehrToken.transfer(_to, amount);
    }
}
