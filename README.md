# DeHR smartcontracts

## DeHR token

DeHR token is a basic KRC20 token:

- Total supply : 1,000,000,000. (1 billion)
- Decimals: 18.

This contract also provides a basic 2/3 multisig features for minting token.

### propose

Propose to mint token to an address.

- `_to`: Token is minted to this address.
- `_amount`: Amount of token.

### accept

Accept a mint token proposal.

- `_id`: proposal id.

### proposeOperator

Propose to set status of an operator.

- `_operator`: Operator address.
- `_status`: Status (1=enable, 2=disable).

### acceptOperator

Accept an operator change status proposal.

- `_id`: proposal id.

## VestingSchedule contract

VestingSchedule is used for distributing token. The distribution is separated to 2 phases:

- Phase1: release 20% after start.
- Phase2: release 10% every month during 8 months.

Steps of operation:

- The operator inputs the list of account and amount of token.
- The operator starts the schedule by calling `startSchedule` function.
- Investor can claim the token manually by calling `claim` function.
- Everyone can call `claimFor` function to claim token for investors.

VestingSchedule also implements the metadata info interface of KRC20 token so that people can at it to kai wallet to see the locked balance.

### setAccount

Record account and amount of token.

- `_acc`: Account address.
- `_amount`: Amount of token.

### setAccountBatch

Record mutiple accounts and amount of token.

- `_accs`: Account address.
- `_amounts`: Amount of token.

### startSchedule

Start running the vesting schedule.

### claim

Investors call this function to claim their token.

### claimFor

Call this function to claim token for investors.

- `_accounts`: List of investors' address.

# Build

### Prerequisite

Install `nodejs`, `truffle` framework.

### Build

```
npm install
truffle build
```

The `bytecote` should be found in `./build/contracts/DeHR.json`.

### Create one-file smart contract

- Create one-file smart contract by running the below command. `DeHR-merge.sol` file should be created at the project's root directory.

```
npm run merge
```

# Vesting

## Description

- This contract implements token vesting schedule.
- The schedule is separated into many unlocks.
- The first unlock time is called `cliff`.
- Only owner is able to withdraw token.

## Constructor

IERC20 \_dehr: DeHr token address
uint256 \_cliffDurationMonth: month of cliff
uint256 \_cliffReleasePercent: cliff release percent
uint256 \_periodDurationMonth: how long per period
uint256 \_periodReleasePercent: percent of period release
uint256 \_totalLockAmount: total token lock amount
string memory \_name: contract name

## Vesting examples

- Eg 1:

```
5 unlocks: each unlock with 20% of tokens

1st unlock: 6 months after start vesting

2nd unlock: 6 months after start vesting

3rd unlock: 9 months after start vesting

4th unlock: 12 months after start vesting

5th unlock: 15 months after start vesting
```

```
uint256 _cliffDurationMonth: 6
uint256 _cliffReleasePercent: 20%
uint256 _periodDurationMonth: 3
uint256 _periodReleasePercent: 20%
```

- Notes: Withdrawal action is locked by the lastWithdraw + \_periodDurationMonth

## Operation

- Deploy contract
- Call startVesting(start date timestamp)
- Call withdraw to withdraw unlock token

# Distributor

## Description

- This contract is used to distributed token to many addresses in just one transaction to save gas.
- Owner call distribute function with list of address and coresponded amount.

# Deploy

### Merge contract to one file

```
truffle-flattener contracts/Vesting.sol
```

### Using Kai wallet to deploy
