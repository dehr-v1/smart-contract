
// https://github.com/pubkey/eth-crypto/blob/master/tutorials/encrypted-message.md

const {
    BN,           // Big Number support
    constants,    // Common constants, like the zero address and largest integers
    expectEvent,  // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
    time,
  } = require('@openzeppelin/test-helpers');
const { assertion } = require('@openzeppelin/test-helpers/src/expectRevert');

  const EthUtils = require("ethereumjs-util");


const Staking = artifacts.require('Staking');
const DeHR = artifacts.require('DeHR');

contract('staking', ([owner, acc1, acc2, acc3, recruiter, jobseeker, operator, feeAddr]) => {
    const ONE_MONTH_DURATION = 60*60*24*30; 
    const decimals = new BN('18', 10);
    const TEN = new BN('10', 10);
    const DECIMALS = TEN.pow(decimals);
    const TOTAL_SUPPLY = (new BN('1000000000', 10)).mul(DECIMALS); // 1B
    const PRIVATE_SALE_AMOUNT = DECIMALS.mul(new BN('200000000', 10)); //200M

    const feePercent = new BN('100', 10); // 1%
    const FEE_BASE = new BN('10000', 10);

    var staking; 

    beforeEach( async () => {
        dehr = await DeHR.new(acc1, acc2, acc3);
        staking = await Staking.new(dehr.address);

        await dehr.propose(recruiter, PRIVATE_SALE_AMOUNT, {from: acc1});
        await dehr.accept(0, {from: acc2});

        await dehr.approve(staking.address, TOTAL_SUPPLY, {from:recruiter})

        await dehr.transfer(jobseeker, decimals.div(new BN(10, 10)), {from: recruiter});
    });

    it('Should stake successfully', async () => {
        // Can not stake over balance
        await expectRevert(staking.stake(PRIVATE_SALE_AMOUNT.add(new BN(1, 10)), {from: recruiter}), "ERC20: transfer amount exceeds balance");

        // Stake successfully
        var stakeTx = await staking.stake('10', {from: recruiter});

        var stakeBlock = await web3.eth.getBlock(stakeTx.receipt.blockNumber)
        var expecLockTo = (parseInt(stakeBlock.timestamp) + ONE_MONTH_DURATION)+''
        
        expectEvent(stakeTx, 'Stake', {addr: recruiter, amount: '10', totalAmount: '10', lockTo: expecLockTo});

        var stakeInfo1 = await staking.stakeInfo.call(recruiter);
        assert.equal(expecLockTo, stakeInfo1.lockTo.toString());
        assert.equal('10', stakeInfo1.amount.toString());

        // 2nd stake successfully
        var stakeTx2 = await staking.stake('20', {from: recruiter});

        var stakeBlock2 = await web3.eth.getBlock(stakeTx2.receipt.blockNumber)
        var expecLockTo = (parseInt(stakeBlock2.timestamp) + ONE_MONTH_DURATION)+''
        var stakeInfo2 = await staking.stakeInfo.call(recruiter);
        assert.equal(expecLockTo, stakeInfo2.lockTo.toString());
        assert.equal('30', stakeInfo2.amount.toString());
    });

    it('Should unstake successfully', async () => {
        // Stake successfully
        var stakeTx = await staking.stake('100', {from: recruiter});

        var stakeBlock = await web3.eth.getBlock(stakeTx.receipt.blockNumber)
        var expecLockTo = (parseInt(stakeBlock.timestamp) + ONE_MONTH_DURATION)+''
        var stakeInfo1 = await staking.stakeInfo.call(recruiter);
        assert.equal(expecLockTo, stakeInfo1.lockTo.toString());
        assert.equal('100', stakeInfo1.amount.toString());

        await time.increase(1);

        // Can not unstake if not over 30 day
        await expectRevert(staking.unstake('20', {from: recruiter}), "LOCKED");

        await time.increase(ONE_MONTH_DURATION);

        // unstake successfully
        var unstakeTx1 = await staking.unstake('30', {from: recruiter});
        expectEvent(unstakeTx1, 'Unstake', {addr: recruiter, amount: '30', totalAmount: '70'});

        var stakeInfo2 = await staking.stakeInfo.call(recruiter);
        assert.equal(expecLockTo, stakeInfo2.lockTo.toString());
        assert.equal('70', stakeInfo2.amount.toString());

        // 2nd unstake successfully
        var unstakeTx2 = await staking.unstake('20', {from: recruiter});
        expectEvent(unstakeTx2, 'Unstake', {addr: recruiter, amount: '20', totalAmount: '50'});

        var stakeInfo3 = await staking.stakeInfo.call(recruiter);
        assert.equal(expecLockTo, stakeInfo3.lockTo.toString());
        assert.equal('50', stakeInfo3.amount.toString());

        // Can not unstake amount that is larger than staked amount
        await expectRevert(staking.unstake('120', {from: recruiter}), "INVALID AMOUNT");
    });

    it('owner functions', async () => {
        // change staking duration
        await expectRevert(staking.changeStakingDuration(10, {from:recruiter}), "Ownable: caller is not the owner");
        await staking.changeStakingDuration(10);
        assert.equal('10', (await staking.duration.call()))

        // Change staking token
        await expectRevert(staking.changeStakeToken(staking.address, {from: recruiter}), "Ownable: caller is not the owner");
        await staking.changeStakeToken(staking.address);
        assert.equal(staking.address, (await staking.dehrToken.call()));
    })
});