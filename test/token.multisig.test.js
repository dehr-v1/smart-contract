

const {
    BN,           // Big Number support
    constants,    // Common constants, like the zero address and largest integers
    expectEvent,  // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
    time,
  } = require('@openzeppelin/test-helpers');


const DeHR = artifacts.require('DeHR');

contract('Vesting', ([owner, acc1, acc2, acc3, dev, minter]) => {
    const ONE_MONTH_DURATION = 60*60*24*30; 
    const PRIVATE_SALE_AMOUNT = new BN('200000000000000000000'); //200M
    const decimals = new BN('18', 10);
    const TEN = new BN('10', 10);
    const TOTAL_SUPPLY = (new BN('1000000000', 10)).mul(TEN.pow(decimals)); // 1B


    var dehr ; 
    var vesting;
    beforeEach('should mint 1B token when deploying', async () => {
        dehr = await DeHR.new(acc1, acc2, acc3);
    });

    it('Should propose and accept mint token successfully ', async () => {
        // Revert if caller is not operator
        await expectRevert(dehr.propose(dev, 10, {from: dev}), "NOT OPERATOR");
        await expectRevert(dehr.propose(dev, 10, {from: dev}), "NOT OPERATOR");
        await dehr.propose(dev, 10, {from: acc1});
        
        var p0 = await dehr.proposals.call(0)

        assert.equal(p0.to, dev, "wrong to address");
        assert.equal(p0.amount, 10, "wrong amount");
        assert.equal(p0.agreeCount, 1, "wrong agree");

        // Revert when accept by not operator
        await expectRevert(dehr.accept(0, {from: dev}), "NOT OPERATOR");

        // Revert when accept by the proposer
        await expectRevert(dehr.accept(0, {from: acc1}), "INVALID SENDER");

        // Accept by operators
        await dehr.accept(0, {from: acc2});

        var p0 = await dehr.proposals.call(0)

        assert.equal(p0.to, dev, "wrong to address");
        assert.equal(p0.amount, 10, "wrong amount");
        assert.equal(p0.agreeCount, 2, "wrong agree");

        var devB = await dehr.balanceOf.call(dev);
        assert.equal(devB.toString(10), '10', "wrong balance");
    });

    it('Should propose and accept operator successfully ', async () => {
        // Revert if caller is not operator
        await expectRevert(dehr.proposeOperator(dev, 1, {from: dev}), "NOT OPERATOR");

        // Revert if status is neither 1 or 2
        await expectRevert(dehr.proposeOperator(dev, 3, {from: acc1}), "INVALID");
        await expectRevert(dehr.proposeOperator(dev, 0, {from: acc1}), "INVALID");

        await dehr.proposeOperator(dev, 1, {from: acc1});
        
        var p0 = await dehr.changeOperatorProposals.call(0)

        assert.equal(p0.to, dev, "wrong to address");
        assert.equal(p0.amount, 1, "wrong status");
        assert.equal(p0.agreeCount, 1, "wrong agree");

        // // Revert when accept by not operator
        await expectRevert(dehr.acceptOperator(0, {from: dev}), "NOT OPERATOR");

        // Revert when accept by the proposer
        await expectRevert(dehr.acceptOperator(0, {from: acc1}), "INVALID SENDER");

        // Accept by operators
        await dehr.acceptOperator(0, {from: acc2});

        var p0 = await dehr.changeOperatorProposals.call(0)

        assert.equal(p0.to, dev, "wrong to address");
        assert.equal(p0.amount, 1, "wrong amount");
        assert.equal(p0.agreeCount, 2, "wrong agree");

        var devB = await dehr.operators.call(dev);
        assert.equal(devB, true, "wrong status");


        // Remove operator
        await dehr.proposeOperator(dev, 2, {from: acc1});
        await dehr.acceptOperator(1, {from: dev});

        var devB = await dehr.operators.call(dev);
        assert.equal(devB, false, "wrong status");

    });


    it('Should not mint over the max total', async () => {
        var amount500M = TOTAL_SUPPLY.div(new BN(2,10))

        // Mint 500M
        await dehr.propose(dev, amount500M, {from: acc1});
        await dehr.accept(0, {from: acc2});

        var devB = await dehr.balanceOf.call(dev);
        assert.equal(devB.toString(10), amount500M.toString(10), "wrong balance");

        var totalSupply = await dehr.totalSupply.call();
        assert.equal(totalSupply.toString(10), amount500M.toString(10), "wrong total supply");

        // Mint 500M
        await dehr.propose(dev, amount500M, {from: acc1});
        await dehr.accept(1, {from: acc2});

        var totalSupply = await dehr.totalSupply.call();
        assert.equal(totalSupply.toString(10), TOTAL_SUPPLY.toString(10), "wrong total supply");

        // Mint 500M
        await dehr.propose(dev, amount500M, {from: acc1});
        await expectRevert(dehr.accept(2, {from: acc2}), "MAX SUPPLY");


    });

})