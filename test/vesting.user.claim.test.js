

const {
    BN,           // Big Number support
    constants,    // Common constants, like the zero address and largest integers
    expectEvent,  // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
    time,
  } = require('@openzeppelin/test-helpers');

const Vesting = artifacts.require('VestingSchedule');
const DeHR = artifacts.require('DeHR');

contract('Vesting', ([owner, alice, bob, carol, acc1, acc2, acc3, minter]) => {
    const ONE_MONTH_DURATION = 60*60*24*30; 
    const PRIVATE_SALE_AMOUNT = new BN('200000000000000000000'); //200M
    const CLIFF_RELEASE_PERCENT = 2000; // 20%
    const DURATION = 8;
    var dehr ; 
    var vesting;
    var name = "DeHR-Lock1";
    var symbol = "DeHRL1";

    before('should mint 1B token when deploying', async () => {
        dehr = await DeHR.new(acc1, acc2, acc3);
        vesting = await Vesting.new(
            dehr.address,
            DURATION,
            CLIFF_RELEASE_PERCENT,
            name,
            symbol
        );

        await dehr.propose(vesting.address, PRIVATE_SALE_AMOUNT, {from: acc1});
        await dehr.accept(0, {from: acc2});
    });

    it('add', async () => {
        await vesting.setAccount(alice, 100)
        await vesting.setAccountBatch([bob, carol], [99, 111]);

        var aliceA = await vesting.balanceOf(alice);
        var bobA = await vesting.balanceOf(bob);
        var carolA = await vesting.balanceOf(carol);

        assert.equal("100", aliceA.toString(10))
        assert.equal("99", bobA.toString(10))
        assert.equal("111", carolA.toString(10))
    });

    it('Should claim successfully ', async()=>{
        await expectRevert(vesting.claim({from:alice}), "NOT_START");
        await vesting.startSchedule(0);

        // Claim 20% after start
        expectEvent((await vesting.claim({from:alice})), "Claim");
        var aliceDeHRA = await dehr.balanceOf(alice);
        assert.equal("20", aliceDeHRA.toString(10));

        // Claim after 1 month
        await time.increase(ONE_MONTH_DURATION);

        expectEvent((await vesting.claim({from:alice})), "Claim");

        var aliceDeHRA = await dehr.balanceOf(alice);
        assert.equal("30", aliceDeHRA.toString(10));

        // Claim after 2 month
        await time.increase(ONE_MONTH_DURATION*2);

        expectEvent((await vesting.claim({from:alice})), "Claim");

        var aliceDeHRA = await dehr.balanceOf(alice);
        assert.equal("50", aliceDeHRA.toString(10));


        // Claim after 1/2 month
        await time.increase(ONE_MONTH_DURATION/2);

        await expectRevert(vesting.claim({from:alice}), "TOO_SMALL");

        var aliceDeHRA = await dehr.balanceOf(alice);
        assert.equal("50", aliceDeHRA.toString(10));

        // Claim all
        await time.increase(ONE_MONTH_DURATION*10);
        expectEvent((await vesting.claim({from:alice})), "Claim");

        assert.equal("100", (await dehr.balanceOf(alice)).toString(10));
        assert.equal("0", (await vesting.balanceOf(alice)).toString(10));

        // Can not claim more
        await time.increase(ONE_MONTH_DURATION);
        await expectRevert(( vesting.claim({from:alice})), "TOO_SMALL");

    });
    
});
