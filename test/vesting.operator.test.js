

const {
    BN,           // Big Number support
    constants,    // Common constants, like the zero address and largest integers
    expectEvent,  // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
    time,
  } = require('@openzeppelin/test-helpers');

const Vesting = artifacts.require('VestingSchedule');
const DeHR = artifacts.require('DeHR');

contract('Vesting', ([owner, alice, bob, carol, acc1, acc2, acc3, dev, minter]) => {
    const ONE_MONTH_DURATION = 60*60*24*30; 
    const PRIVATE_SALE_AMOUNT = new BN('200000000000000000000'); //200M
    const CLIFF_RELEASE_PERCENT = 2000; // 20%
    const DURATION = 8;
    var dehr ; 
    var vesting;
    var name = "DeHR-Lock1";
    var symbol = "DeHRL1";

    beforeEach('should mint 1B token when deploying', async () => {
        dehr = await DeHR.new(acc1, acc2, acc3);
        vesting = await Vesting.new(
            dehr.address,
            DURATION,
            CLIFF_RELEASE_PERCENT,
            name,
            symbol
        );
    });

    it('Should set account successfully', async () => {
        await vesting.setAccount(alice, 100)
        await vesting.setAccountBatch([bob, carol], [99, 111]);

        var aliceA = await vesting.balanceOf(alice);
        var bobA = await vesting.balanceOf(bob);
        var carolA = await vesting.balanceOf(carol);

        assert.equal("100", aliceA.toString(10));
        assert.equal("99", bobA.toString(10));
        assert.equal("111", carolA.toString(10));

        await expectRevert(vesting.setAccount(dev, 200, {from:alice}), "Ownable: caller is not the owner");
        await expectRevert(vesting.setAccountBatch([bob, carol], [99, 111], {from:alice}), "Ownable: caller is not the owner");
    });

    it('Should start successfully', async () => {
        await expectRevert(vesting.startSchedule(0, {from:alice}), "Ownable: caller is not the owner");
        
        expectEvent(await vesting.startSchedule(0), "Start");

        var startDate  = await vesting.startDate.call();
        var currentBlockTime = await time.latest();

        assert.equal(startDate.toString(10), currentBlockTime.toString(10));


        // Can not start twice
        await expectRevert(vesting.startSchedule(0, {from:owner}), "STARTED");
    });


    it('Should start with custom start time successfully', async () => {
        expectEvent(await vesting.startSchedule(11), "Start");

        var startDate  = await vesting.startDate.call();
        assert.equal(startDate.toString(10), "11");
    });


    it('Token metadata', async () => {
        assert.equal((await vesting.name.call()), name);
        assert.equal((await vesting.symbol.call()), symbol);
        assert.equal((await vesting.decimals.call()).toString(10), "18");
    });

});
