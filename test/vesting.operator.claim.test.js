

const {
    BN,           // Big Number support
    constants,    // Common constants, like the zero address and largest integers
    expectEvent,  // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
    time,
  } = require('@openzeppelin/test-helpers');

const Vesting = artifacts.require('VestingSchedule');
const DeHR = artifacts.require('DeHR');

contract('Vesting', ([owner, alice, bob, carol, acc1, acc2, acc3, minter]) => {
    const ONE_MONTH_DURATION = 60*60*24*30; 
    const PRIVATE_SALE_AMOUNT = new BN('200000000000000000000'); //200M
    const CLIFF_RELEASE_PERCENT = 2000; // 20%
    const DURATION = 8;
    var dehr ; 
    var vesting;
    var name = "DeHR-Lock1";
    var symbol = "DeHRL1";

    before('should mint 1B token when deploying', async () => {
        dehr = await DeHR.new(acc1, acc2, acc3);
        vesting = await Vesting.new(
            dehr.address,
            DURATION,
            CLIFF_RELEASE_PERCENT,
            name,
            symbol
        );


        await dehr.propose(vesting.address, PRIVATE_SALE_AMOUNT, {from: acc1});
        await dehr.accept(0, {from: acc2});
    });

    it('add', async () => {
        await vesting.setAccount(alice, 100)
        await vesting.setAccountBatch([bob, carol], [99, 111]);

        var aliceA = await vesting.balanceOf(alice);
        var bobA = await vesting.balanceOf(bob);
        var carolA = await vesting.balanceOf(carol);

        assert.equal("100", aliceA.toString(10))
        assert.equal("99", bobA.toString(10))
        assert.equal("111", carolA.toString(10))
    });

    it('Should claim successfully ', async()=>{
        var claimAddrs = [alice, bob]
        await expectRevert(vesting.claimFor(claimAddrs), "NOT_START");
        await vesting.startSchedule(0);


        // Claim 20% after start
        expectEvent((await vesting.claimFor(claimAddrs)), "Claim");

        // Unlock balance
        assert.equal("20", (await dehr.balanceOf(alice)).toString(10))
        assert.equal("19", (await dehr.balanceOf(bob)).toString(10));
        // Locked balance
        assert.equal((await vesting.balanceOf.call(alice)).toString(10), "80");
        assert.equal((await vesting.balanceOf.call(bob)).toString(10), "80");

        // 1 month
        await time.increase(ONE_MONTH_DURATION);
        expectEvent((await vesting.claimFor(claimAddrs)), "Claim");

        // Unlock balance
        assert.equal("30", (await dehr.balanceOf(alice)).toString(10))
        assert.equal("29", (await dehr.balanceOf(bob)).toString(10));

        // Locked balance
        assert.equal((await vesting.balanceOf.call(alice)).toString(10), "70");
        assert.equal((await vesting.balanceOf.call(bob)).toString(10), "70");

        // 0.5 month
        await time.increase(ONE_MONTH_DURATION/2);
        
        await expectRevert(vesting.claimFor(claimAddrs), "TOO_SMALL");

        // 4.5 month 
        await time.increase(ONE_MONTH_DURATION/2+ONE_MONTH_DURATION*4);
        expectEvent((await vesting.claimFor(claimAddrs)), "Claim");

        // Unlock balance
        assert.equal("80", (await dehr.balanceOf(alice)).toString(10))
        assert.equal("79", (await dehr.balanceOf(bob)).toString(10));

        // Locked balance
        assert.equal((await vesting.balanceOf.call(alice)).toString(10), "20");
        assert.equal((await vesting.balanceOf.call(bob)).toString(10), "20");

        // claim 1 time
        // Locked
        assert.equal("111", (await vesting.balanceOf(carol)).toString(10));

        expectEvent((await vesting.claimFor([carol])), "Claim");

        // Locked
        assert.equal("23", (await vesting.balanceOf(carol)).toString(10))
        // Unlocked
        assert.equal("88", (await dehr.balanceOf(carol)).toString(10));


        // 2 month
        await time.increase(ONE_MONTH_DURATION*2);
        expectEvent((await vesting.claimFor([...claimAddrs,carol])), "Claim");

        assert.equal("0", (await vesting.balanceOf(alice)).toString(10))
        assert.equal("0", (await vesting.balanceOf(bob)).toString(10))
        assert.equal("0", (await vesting.balanceOf(carol)).toString(10))

        assert.equal("100", (await dehr.balanceOf(alice)).toString(10));
        assert.equal("99", (await dehr.balanceOf(bob)).toString(10));
        assert.equal("111", (await dehr.balanceOf(carol)).toString(10));

        // Can not claim more
        // 2 month
        await time.increase(ONE_MONTH_DURATION*2);

        await expectRevert(vesting.claimFor(claimAddrs), "TOO_SMALL");


    });
    
});
