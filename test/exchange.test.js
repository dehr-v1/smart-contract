
// https://github.com/pubkey/eth-crypto/blob/master/tutorials/encrypted-message.md

const {
    BN,           // Big Number support
    constants,    // Common constants, like the zero address and largest integers
    expectEvent,  // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
    time,
  } = require('@openzeppelin/test-helpers');

  const EthUtils = require("ethereumjs-util");


const ExchangeData = artifacts.require('ExchangeData');
const DeHR = artifacts.require('DeHR');

contract('Exchange data', ([owner, acc1, acc2, acc3, recruiter, jobseeker, operator, feeAddr]) => {
    const ONE_MONTH_DURATION = 60*60*24*30; 
    const decimals = new BN('18', 10);
    const TEN = new BN('10', 10);
    const DECIMALS = TEN.pow(decimals);
    const TOTAL_SUPPLY = (new BN('1000000000', 10)).mul(DECIMALS); // 1B
    const PRIVATE_SALE_AMOUNT = DECIMALS.mul(new BN('200000000', 10)); //200M

    const feePercent = new BN('100', 10); // 1%
    const FEE_BASE = new BN('10000', 10);

    var exd; 
    var vesting;
    beforeEach('should mint 1B token when deploying', async () => {
        dehr = await DeHR.new(acc1, acc2, acc3);
        exd = await ExchangeData.new(dehr.address, feePercent, feeAddr);
        await exd.setOperator(operator, true);

        await dehr.propose(recruiter, PRIVATE_SALE_AMOUNT, {from: acc1});
        await dehr.accept(0, {from: acc2});

        await dehr.approve(exd.address, TOTAL_SUPPLY, {from:recruiter})

    });

    it('Should register successfully', async () => {
        var privKey = Buffer.from('3a8e07d28f087e638574441b7464d31273c0423e38ca78776e7fbfcc5cfad159', 'hex');
        var pubKey = EthUtils.privateToPublic(Buffer.from(privKey, 'hex'))
        var addr = '0x' + EthUtils.pubToAddress(pubKey).toString('hex');
        var checksum = Buffer.from(web3.utils.sha3('string').substring(2), 'hex');

        await expectRevert( exd.register(addr, pubKey, checksum, {from: owner}), "NOT OPERATOR");
        expectEvent(await exd.register(addr, pubKey, checksum, {from: operator}), "Register");

        var ad = await exd.accounts.call(addr);
        assert.equal(ad.publicKey, '0x' + pubKey.toString('hex'), 'wrong pubKey')
        assert.equal(ad.dataChecksum, '0x' + checksum.toString('hex'), 'wrong checksum')

        var privKey2 = Buffer.from('3a8e07d28f087e638574441b7464d31273c0423e38ca78776e7fbfcc5cfad158', 'hex');
        var pubKey2 = EthUtils.privateToPublic(Buffer.from(privKey2, 'hex'))
        var addr2 = '0x' + EthUtils.pubToAddress(pubKey2).toString('hex');
        var checksum2 = Buffer.from(web3.utils.sha3('string2').substring(2), 'hex');

        var accs = [addr, addr2];
        var pubs = [pubKey, pubKey2];
        var checksums = [checksum, checksum2];

        await expectRevert(exd.registerBatch(accs, pubs, checksums, {from: owner}), "NOT OPERATOR");
        expectEvent(await exd.registerBatch(accs, pubs, checksums, {from: operator}), "Register");

        var ad2 = await exd.accounts.call(addr2);
        assert.equal(ad2.publicKey, '0x' + pubKey2.toString('hex'), 'wrong pubKey')
        assert.equal(ad2.dataChecksum, '0x' + checksum2.toString('hex'), 'wrong checksum')
    });

    it('Should send, accept, cancel request successfully', async () => {
        var privKey = Buffer.from('3a8e07d28f087e638574441b7464d31273c0423e38ca78776e7fbfcc5cfad159', 'hex');
        var pubKey = EthUtils.privateToPublic(Buffer.from(privKey, 'hex'))
        var addr = '0x' + EthUtils.pubToAddress(pubKey).toString('hex');
        var checksum = Buffer.from(web3.utils.sha3('string').substring(2), 'hex');

        expectEvent(await exd.register(jobseeker, pubKey, checksum, {from: operator}), "Register");
        
        var recruiterBalance = await dehr.balanceOf.call(recruiter);
        var jobSeekerBalance = await dehr.balanceOf.call(jobseeker);

        // Send request
        var requestAmount = DECIMALS.mul(new BN('20', 10)); //20
        await exd.updatePrice(requestAmount, {from: jobseeker})
        expectEvent(await exd.request1(jobseeker, {from: recruiter}), "SendRequest1");
        assert.equal(recruiterBalance.sub(requestAmount).toString(10), (await dehr.balanceOf.call(recruiter)).toString(10))

        // Can not accept another person's request
        await expectRevert(exd.accept(0, {from: owner}), "INVALID RECIPIENT");

        // Accept
        var feeAmount = requestAmount.mul(feePercent).div(FEE_BASE)
        var expectedBalance = requestAmount.sub(feeAmount);
        expectEvent(await exd.accept(0, {from: jobseeker}), "AcceptRequest");
        assert.equal(jobSeekerBalance.add(expectedBalance).toString(10), (await dehr.balanceOf.call(jobseeker)).toString(10))
        assert.equal(feeAmount.toString(10), (await dehr.balanceOf.call(feeAddr)).toString(10))
        assert.equal('200000000000000000', feeAmount.toString(10))
        
        // Can not cancel request when the request is accepted
        await expectRevert(exd.cancelRequest(0, {from:recruiter}), "INVALID STATUS");

        // Can not accept request twice
        await expectRevert(exd.accept(0, {from:jobseeker}), "INVALID STATUS");


        var requestAmount30 = DECIMALS.mul(new BN('30', 10)); 
        expectEvent(await exd.request1(jobseeker, {from: recruiter}), "SendRequest1");
        
        // Can not cancel another person's request
        await expectRevert(exd.cancelRequest(1, {from: owner}), "INVALID SENDER");

        // Cancel request
        expectEvent(await exd.cancelRequest(1, {from: recruiter}), "CancelRequest");
    });

    it('Should succeed to send Request2', async () => {
        var privKey = Buffer.from('3a8e07d28f087e638574441b7464d31273c0423e38ca78776e7fbfcc5cfad159', 'hex');
        var pubKey = EthUtils.privateToPublic(Buffer.from(privKey, 'hex'))
        var addr = '0x' + EthUtils.pubToAddress(pubKey).toString('hex');
        var checksum = Buffer.from(web3.utils.sha3('string').substring(2), 'hex');

        expectEvent(await exd.register(jobseeker, pubKey, checksum, {from: operator}), "Register");
        
        var recruiterBalance = await dehr.balanceOf.call(recruiter);
        var jobSeekerBalance = await dehr.balanceOf.call(jobseeker);
        var feeBalance = await dehr.balanceOf.call(feeAddr);

        // Send request
        var requestAmount = DECIMALS.mul(new BN('20', 10)); //20
        await exd.updatePrice(requestAmount, {from: jobseeker})
        var feeAmount = requestAmount.mul(feePercent).div(FEE_BASE)

        expectEvent(await exd.request2(jobseeker, {from: recruiter}), "SendRequest2");
        assert.equal(recruiterBalance.sub(requestAmount).toString(10), (await dehr.balanceOf.call(recruiter)).toString(10))
        assert.equal(jobSeekerBalance.add(requestAmount.sub(feeAmount)).toString(10), (await dehr.balanceOf.call(jobseeker)).toString(10))
        assert.equal(feeBalance.add(feeAmount).toString(10), (await dehr.balanceOf.call(feeAddr)).toString(10))

        // Can not accept request2
        await expectRevert(exd.accept(0, {from: jobseeker}), "INVALID STATUS");
    })


    it('Should set fee address successfully', async () => {
        await expectRevert(exd.setFeeAddress(owner, {from: acc1}), "NOT OPERATOR");
        await exd.setFeeAddress(owner, {from: operator});
        assert.equal(owner, (await exd.feeAddress.call()))
    });

    it('Should set fee percent successfully', async () => {
        await expectRevert(exd.setFeePercent(200, {from: acc1}), "NOT OPERATOR");
        await exd.setFeePercent(200, {from: operator});
        assert.equal('200', (await exd.feePercent.call()).toString())
    });

    it('Should set operator address successfully', async () => {
        await expectRevert(exd.setOperator(acc2, true, {from: acc1}), "Ownable: caller is not the owner");
        await exd.setOperator(acc2, true, {from: owner});
        assert.equal(true, (await exd.operators.call(acc2)));

        await exd.setOperator(acc2, false, {from: owner});
        assert.equal(false, (await exd.operators.call(acc2)))

    });
});