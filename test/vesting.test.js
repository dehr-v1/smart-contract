

const {
    BN,           // Big Number support
    constants,    // Common constants, like the zero address and largest integers
    expectEvent,  // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
    time,
  } = require('@openzeppelin/test-helpers');

const Vesting = artifacts.require('Vesting');
const DEHR = artifacts.require("ERC20Token");

contract('vesting', ([owner, alice, bob, carol, dave, acc1, acc2, acc3, keeper]) => {
    const ONE_MONTH_DURATION = new BN(60*60*24*30, 10); 
    const PRIVATE_SALE_AMOUNT = new BN('200000000000000000000'); //200M
    const CLIFF_DURATION = new BN(6, 10);
    const CLIFF_RELEASE_PERCENT = new BN(1250, 10); // 12.5%
    const TOTAL_PERIODS = new BN(18, 10);
    const PERIOD_DURATION = new BN(3, 10);
    const PERIOD_RELEASE_PERCENT = new BN(1250, 10); //12.5
    const BASE_PERCENT = new BN(10000, 10);
    const BN2 = new BN(2, 10);
    var dehrToken ; 
    var vesting;
    var name = "Team fund";

    var aliceAmount = new BN(9900, 10);
    var bobAmount = new BN(10000, 10);
    var carolAmount = new BN(11000, 10);
    var daveAmount = new BN(20000, 10);

    beforeEach(async () => {
        dehrToken = await DEHR.new();
        vesting = await Vesting.new(
            dehrToken.address,
            CLIFF_DURATION,
            CLIFF_RELEASE_PERCENT,
            PERIOD_DURATION,
            PERIOD_RELEASE_PERCENT,
            PRIVATE_SALE_AMOUNT,
            name
        );

        // Send fund to vesting
        await dehrToken.transfer(vesting.address, PRIVATE_SALE_AMOUNT);
    });

    it('Start without specific day', async () => {
        await expectRevert(vesting.startVesting(0, {from: alice}), "Ownable: caller is not the owner");

        var tx = await vesting.startVesting(0);
        var startBlock = await web3.eth.getBlock(tx.receipt.blockNumber);

        assert.equal(startBlock.timestamp, (await vesting.startDate.call()),toString())
    });
    it('Start with specific day', async () => {
        var startDate = '1634586838'
        await expectRevert(vesting.startVesting(startDate, {from: alice}), "Ownable: caller is not the owner");

        var tx = await vesting.startVesting(startDate);

        assert.equal(startDate, (await vesting.startDate.call()),toString())
    });

    it('Can not withdraw', async () => {
        // not started
        await expectRevert(vesting.withdraw(keeper), "NOT START");
        
        var tx = await vesting.startVesting(0);
        // not mature
        await time.increase(ONE_MONTH_DURATION);

        await expectRevert(vesting.withdraw(keeper), "NOT MATURE");
    })

    it('Withdraw successfully', async () => {
        await vesting.startVesting(0);

        // Cliff withdraw
        await time.increase(ONE_MONTH_DURATION.mul(CLIFF_DURATION));
        await time.increase(1);
        var tx = await vesting.withdraw(keeper);
        var expectedAmount = PERIOD_RELEASE_PERCENT.mul(PRIVATE_SALE_AMOUNT).div(BASE_PERCENT);
        expectEvent(tx, 'Withdraw', {to: keeper, amount: expectedAmount.toString()});
        assert.equal(expectedAmount.toString(), (await dehrToken.balanceOf.call(keeper)).toString());
        
        var block = await web3.eth.getBlock(tx.receipt.blockNumber);
        assert.equal(block.timestamp, (await vesting.lastWithdrawTime.call()).toString());
        
        assert.equal(PERIOD_DURATION.mul(ONE_MONTH_DURATION).add(new BN(block.timestamp, 10)).toString(), (await vesting.getWithdrawDate.call()).toString());
        
        // next
        await expectRevert(vesting.withdraw(acc1), "NOT MATURE");
        await time.increase(ONE_MONTH_DURATION.mul(PERIOD_DURATION));
        await time.increase(1);
        var expectedAmount = '25000000000000000000';
        var tx = await vesting.withdraw(acc1);
        expectEvent(tx, 'Withdraw', {to: acc1, amount: expectedAmount});
        assert.equal(expectedAmount, (await dehrToken.balanceOf.call(acc1)).toString());

        // next
        await expectRevert(vesting.withdraw(acc2), "NOT MATURE");
        await time.increase(ONE_MONTH_DURATION.mul(PERIOD_DURATION));
        await time.increase(1);
        var expectedAmount = '25000000000000000000';
        var tx = await vesting.withdraw(acc2);
        expectEvent(tx, 'Withdraw', {to: acc2, amount: expectedAmount});
        assert.equal(expectedAmount, (await dehrToken.balanceOf.call(acc2)).toString());
    })

    it('Only owner can drain token', async () => {
        await expectRevert(vesting.withdrawTokenByOwner(acc2, {from: acc2}), "Ownable: caller is not the owner");
        await vesting.withdrawTokenByOwner(acc2);
        assert.equal(PRIVATE_SALE_AMOUNT.toString(), (await dehrToken.balanceOf.call(acc2)).toString());
    })
});
